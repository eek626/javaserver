package com.richardcheng.javaserver;

import java.io.File;
import java.util.Hashtable;

public class FileHelper {
    private File[] files;

    public FileHelper(File[] files) {
        this.files = files;
    }

    public Hashtable<String, Object> listMap() {
        Hashtable<String, Object> list = new Hashtable<>();

        for( File file : files) {
            list.put(file.getName(), 1);
        }

        return list;
    }
}
