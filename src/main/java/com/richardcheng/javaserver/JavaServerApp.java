package com.richardcheng.javaserver;

import com.richardcheng.endpoint.EndpointFactory;
import com.richardcheng.endpoint.IEndpoint;
import com.richardcheng.httpIO.HttpRequest;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Hashtable;

public class JavaServerApp {
    private static ServerArgumentHelper serverArgs;
    private static Hashtable<String, Object> directoryList = new Hashtable<>();
    private static ServerSocket serverSocket = null;
    private static Hashtable<String, IEndpoint> endpoints;
    private static Server server;

    public static void main(String[] args) {
        if (init(args)) {
            consoleStartupMessages();
            server.run(serverArgs.port(), true);
        }
        consoleStopMessage();
    }

    public static boolean init(String[] args) {
        serverArgs = new ServerArgumentHelper(args);

        if (serverArgs.pathLength() > 0) {
            File[] directory = new File(serverArgs.path()).listFiles();
            directoryList = new FileHelper(directory).listMap();
        }

        try {
            serverSocket = new ServerSocket();
        } catch (IOException e) {
            System.out.println("Error: could not create ServerSocket");
            return false;
        }

        endpoints = EndpointFactory.create(serverArgs.port(), serverArgs.path(), directoryList);
        server = new Server(serverSocket, new Controller(endpoints, directoryList), new HttpRequest());

        return true;
    }

    public static void consoleStartupMessages() {
        System.out.println("Starting Java Server");
        System.out.println("Using port: " + serverArgs.port());
        System.out.println("Using contents at path: " + serverArgs.path() + "as root endpoint list");
        System.out.println("Server started, go to http://localhost:"+serverArgs.port());
    }

    public static void consoleStopMessage() {
        System.out.println("Java Server not started due to error creating socket," +
                "please check system settings and try launching Java Server again. Good bye.");
    }
}
