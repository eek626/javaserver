package com.richardcheng.javaserver;

import com.richardcheng.httpIO.HttpRequest;
import com.richardcheng.endpoint.IEndpoint;

import java.util.Hashtable;

public class Controller {
    private Hashtable<String, IEndpoint> endpoints;
    private Hashtable<String, Object> directoryList;

    public Controller(Hashtable<String, IEndpoint> endpoints, Hashtable<String, Object> directoryList) {
        this.endpoints = endpoints;
        this.directoryList = directoryList;
    }

    public byte[] routeRequest(HttpRequest request) {
        String requestEndpoint = request.getEndpoint();
        IEndpoint endpoint = endpoints.get(requestEndpoint);
        Object file = directoryList.get(requestEndpoint);

        if (endpoint == null) {
            if (file == null) {
                endpoint = endpoints.get("invalid");
            } else {
                endpoint = endpoints.get("dynamic");
            }
        }

        return endpoint.route(request);
    }
}
