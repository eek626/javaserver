package com.richardcheng.javaserver;

import com.richardcheng.httpIO.HttpRequest;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;


public class Server {
    private Controller controller;
    private HttpRequest request;
    private ServerSocket serverSocket;
    private Socket requestSocket;
    private boolean requiresResponse;

    public Server(ServerSocket serverSocket, Controller controller, HttpRequest request) {
        this.controller = controller;
        this.request = request;
        this.serverSocket = serverSocket;
    }

    public void run(int port, boolean continuousRun) {
        try {
            this.init(port);
            do {
                requestSocket = serverSocket.accept();
                requiresResponse = this.request(requestSocket);
                if (requiresResponse) {
                    this.response(requestSocket);
                }
            } while (continuousRun);
        } catch (IOException e) {
            System.out.println("Error: IOException occured at Server#run");
        }
    }

    private void init(int port) throws IOException{
        serverSocket.setReuseAddress(true);
        serverSocket.bind(new InetSocketAddress(port));
    }

    public void stop() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            System.out.println("Socket already closed!");
        }
    }

    private boolean request(Socket requestSocket) throws IOException{
        BufferedReader requestMessage;

        requestMessage = new BufferedReader(new InputStreamReader(requestSocket.getInputStream()));

        return request.parseMessage(requestMessage);
    }

    private void response(Socket requestSocket) throws IOException{
        byte[] response = controller.routeRequest(request);

        DataOutputStream responseStream = new DataOutputStream(requestSocket.getOutputStream());
        responseStream.write(response);
        responseStream.close();
    }
}
