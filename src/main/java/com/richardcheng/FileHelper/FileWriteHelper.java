package com.richardcheng.FileHelper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriteHelper {
    private File file;
    private FileWriter fileWriter;
    private BufferedWriter bufferedWriter;

    public FileWriteHelper() {
    }

    public void init(String fullPath) {
        file = new File(fullPath);
        try {
            fileWriter = new FileWriter(file, false);
            bufferedWriter = new BufferedWriter(fileWriter);
        } catch (IOException e) {
            bufferedWriter = null;
        }
    }

    public void setBufferedWriter(BufferedWriter bufferedWriter) {
        this.bufferedWriter = bufferedWriter;
    }

    public BufferedWriter getBufferedWriter() {
        return bufferedWriter;
    }

    public boolean write(String message) {
        try {
            bufferedWriter.write(message);
            bufferedWriter.close();
            return true;
        } catch (IOException e) {
            System.out.println("Error: Can't write to bufferedWriter.");
            return false;
        }
    }
}
