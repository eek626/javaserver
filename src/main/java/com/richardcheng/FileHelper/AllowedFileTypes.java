package com.richardcheng.FileHelper;

public class AllowedFileTypes {
    public static final String File = "file";
    public static final String Gif = "gif";
    public static final String Jpeg = "jpeg";
    public static final String Jpg = "jpg";
    public static final String Png = "png";
    public static final String Txt = "txt";
}
