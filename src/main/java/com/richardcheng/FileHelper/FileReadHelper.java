package com.richardcheng.FileHelper;

import java.io.*;

public class FileReadHelper {
    private static String dash = "-";

    private File file;
    private int fileSize;
    private int start;
    private int stop;
    private int readSize;
    private FileInputStream fileInputStream;

    public FileReadHelper() {
    }

    public void init(String fullPath) {
        file = new File(fullPath);
        fileSize = (int)(long)file.length();
        readSize = fileSize;
        start = 0;
        stop = 0;

        try {
            fileInputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            System.out.println("Error: file not found.");
            fileInputStream = null;
        }
    }

    public void parseRange(String range) {
        String[] splitRange = range.split(dash);
        stop = fileSize - 1;

        try {
            if (range.startsWith(dash)) {
                start = fileSize - Integer.parseInt(splitRange[1]);
            } else if (range.contains(dash) && !range.endsWith(dash)) {
                start = Integer.parseInt(splitRange[0]);
                stop = Integer.parseInt(splitRange[1]);
            } else {
                start = Integer.parseInt(splitRange[0]);
            }
            readSize = stop - start + 1;
        } catch (NumberFormatException e) {
            System.out.println("Error: Bad file range. Not Int.");
            readSize = 0;
        }
    }

    public byte[] readBytes() {
        byte[] content = new byte[readSize];

        try {
            fileInputStream.skip(start);
            fileInputStream.read(content);

            fileInputStream.close();

        } catch (IOException e) {
            System.out.println("Error: reading file.");
            content = null;
        }

        return content;
    }

    public void setFileInputStream(FileInputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public void setReadSize(int readSize) {
        this.readSize = readSize;
    }

    public void setStop(int stop) {
        this.stop = stop;
    }

    public FileInputStream getFileInputStream() {
        return this.fileInputStream;
    }

    public int getStart() {
        return start;
    }

    public int getStop() {
        return stop;
    }

    public int getReadSize() {
        return readSize;
    }

    public int getFileSize() {
        return fileSize;
    }
}
