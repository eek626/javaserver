package com.richardcheng.endpoint;

import com.richardcheng.httpIO.HttpRequest;

public interface IEndpoint {
    byte[] route(HttpRequest request);
}
