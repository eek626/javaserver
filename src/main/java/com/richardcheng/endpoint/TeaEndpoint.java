package com.richardcheng.endpoint;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpRequest;
import com.richardcheng.httpIO.HttpResponse;

import java.util.Hashtable;

public class TeaEndpoint implements IEndpoint {
    private Hashtable<String, String> allowedMethods;

    public TeaEndpoint() {
        allowedMethods = new Hashtable<>();
        allowedMethods.put(HttpMethod.Get, HttpMethod.Status200);
    }

    public byte[] route(HttpRequest httpRequest) {
        String httpMethod = httpRequest.getMethod();
        String statusCode = allowedMethods.get(httpMethod);

        if (statusCode == null) {
            return HttpResponse.statusLine(HttpMethod.Status405).getBytes();
        }

        return HttpResponse.statusLine(statusCode).getBytes();
    }
}
