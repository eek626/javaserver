package com.richardcheng.endpoint;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpResponse;
import com.richardcheng.httpIO.HttpRequest;

public class LogsEndpoint implements IEndpoint {

    public LogsEndpoint() {
    }

    public byte[] route(HttpRequest httpRequest) {
        if (httpRequest.getAuth().equals("admin:hunter2")) {
            return HttpResponse.completeResponse(HttpMethod.Status200, httpRequest.getLog()).getBytes();
        }

        return (HttpResponse.statusLine(HttpMethod.Status401) + HttpResponse.authHeader()).getBytes();
    }
}
