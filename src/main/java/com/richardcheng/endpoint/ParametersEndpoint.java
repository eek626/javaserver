package com.richardcheng.endpoint;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpRequest;
import com.richardcheng.httpIO.HttpResponse;
import com.richardcheng.utils.URLDecoder;

import java.util.Hashtable;

public class ParametersEndpoint implements IEndpoint {
    private Hashtable<String, String> allowedMethods;

    public ParametersEndpoint () {
        allowedMethods = new Hashtable<>();
        allowedMethods.put(HttpMethod.Get, HttpMethod.Status200);
    }

    public byte[] route(HttpRequest httpRequest) {
        String httpMethod = httpRequest.getMethod();
        String statusCode = allowedMethods.get(httpMethod);

        if (statusCode == null) {
            return HttpResponse.statusLine(HttpMethod.Status405).getBytes();
        }

        String encodedParams = httpRequest.getParameters();

        String decodedParams = new URLDecoder().decode(encodedParams);

        return HttpResponse.completeResponse(statusCode, decodedParams + "\r\n").getBytes();
    }
}
