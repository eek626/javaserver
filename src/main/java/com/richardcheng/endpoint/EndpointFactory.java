package com.richardcheng.endpoint;

import com.richardcheng.FileHelper.FileReadHelper;
import com.richardcheng.FileHelper.FileWriteHelper;
import com.richardcheng.presenter.Presenter;

import java.util.Hashtable;

public class EndpointFactory {
    public static Hashtable<String, IEndpoint> create(int port,
                         String path,
                         Hashtable<String, Object> directoryList) {
        Hashtable<String, IEndpoint> endpoints = new Hashtable<>();
        endpoints.put("root", new RootEndpoint(new Presenter(directoryList)));
        endpoints.put("form", new FormEndpoint());
        endpoints.put("coffee", new CoffeeEndpoint());
        endpoints.put("tea", new TeaEndpoint());
        endpoints.put("method_options", new MethodOptionsEndpoint());
        endpoints.put("method_options2", new MethodOptions2Endpoint());
        endpoints.put("redirect", new RedirectEndpoint(port));
        endpoints.put("logs", new LogsEndpoint());
        endpoints.put("parameters", new ParametersEndpoint());
        endpoints.put("dynamic", new DynamicEndpoint(directoryList, path, new FileReadHelper(), new FileWriteHelper()));
        endpoints.put("invalid", new InvalidEndpoint());

        return endpoints;
    }
}
