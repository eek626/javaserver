package com.richardcheng.endpoint;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpResponse;
import com.richardcheng.httpIO.HttpRequest;

import java.util.Hashtable;

public class RedirectEndpoint implements IEndpoint {
    private Hashtable<String, String> allowedMethods;
    private int port;

    public RedirectEndpoint(int port) {
        allowedMethods = new Hashtable<>();
        allowedMethods.put(HttpMethod.Get, HttpMethod.Status302);
        this.port = port;
    }

    public byte[] route(HttpRequest httpRequest) {
        String httpMethod = httpRequest.getMethod();
        String statusCode = allowedMethods.get(httpMethod);

        if (statusCode == null) {
            return HttpResponse.statusLine(HttpMethod.Status405).getBytes();
        }

        return (HttpResponse.statusLine(statusCode) + HttpResponse.location(port)).getBytes();
    }
}
