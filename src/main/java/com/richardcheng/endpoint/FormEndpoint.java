package com.richardcheng.endpoint;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpRequest;
import com.richardcheng.httpIO.HttpResponse;

import java.util.Hashtable;

public class FormEndpoint implements IEndpoint {
    private Hashtable<String, String> allowedMethods;
    private String data = "";

    public FormEndpoint() {
        allowedMethods = new Hashtable<>();
        allowedMethods.put(HttpMethod.Get, HttpMethod.Status200);
        allowedMethods.put(HttpMethod.Put, HttpMethod.Status200);
        allowedMethods.put(HttpMethod.Post, HttpMethod.Status200);
        allowedMethods.put(HttpMethod.Delete, HttpMethod.Status200);
    }

    public byte[] route(HttpRequest httpRequest) {
        String httpMethod = httpRequest.getMethod();
        String statusCode = allowedMethods.get(httpMethod);

        if (statusCode == null) {
            return HttpResponse.statusLine(HttpMethod.Status405).getBytes();
        }

        if (canPost(httpMethod) || (canPut(httpMethod))) {
            this.data = httpRequest.getData();
        } else if (httpMethod.equals(HttpMethod.Delete)) {
            this.data = "";
        }

        return HttpResponse.completeResponse(statusCode, this.data).getBytes();
    }

    private boolean canPost(String httpMethod) {
        return (httpMethod.equals(HttpMethod.Post) && data.length() == 0);
    }

    private boolean canPut(String httpMethod) {
        return (httpMethod.equals(HttpMethod.Put) && data.length() > 0);
    }
}
