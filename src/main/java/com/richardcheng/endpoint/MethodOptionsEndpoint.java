package com.richardcheng.endpoint;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpRequest;
import com.richardcheng.httpIO.HttpResponse;

import java.util.Hashtable;

public class MethodOptionsEndpoint implements IEndpoint {
    private Hashtable<String, String> allowedMethods;

    public MethodOptionsEndpoint() {
        allowedMethods = new Hashtable<>();
        allowedMethods.put(HttpMethod.Get, HttpMethod.Status200);
        allowedMethods.put(HttpMethod.Put, HttpMethod.Status200);
        allowedMethods.put(HttpMethod.Post, HttpMethod.Status200);
        allowedMethods.put(HttpMethod.Head, HttpMethod.Status200);
        allowedMethods.put(HttpMethod.Options, HttpMethod.Status200);
    }

    public byte[] route(HttpRequest httpRequest) {
        String statusCode = allowedMethods.get(httpRequest.getMethod());

        if (statusCode == null) {
            return HttpResponse.statusLine(HttpMethod.Status405).getBytes();
        }

        return (HttpResponse.statusLine(statusCode) + HttpResponse.allowHeader(allowedMethods)).getBytes();
    }
}
