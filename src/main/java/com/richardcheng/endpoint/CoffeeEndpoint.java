package com.richardcheng.endpoint;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpRequest;
import com.richardcheng.httpIO.HttpResponse;

import java.util.Hashtable;

public class CoffeeEndpoint implements IEndpoint {
    private Hashtable<String, String> allowedMethods;

    public CoffeeEndpoint() {
        allowedMethods = new Hashtable<>();
        allowedMethods.put(HttpMethod.Get, HttpMethod.Status418);
    }

    public byte[] route(HttpRequest httpRequest) {
        String httpMethod = httpRequest.getMethod();
        String statusCode = allowedMethods.get(httpMethod);

        if (statusCode == null) {
            return HttpResponse.statusLine(HttpMethod.Status405).getBytes();
        }

        return HttpResponse.completeResponse(statusCode, "I'm a teapot").getBytes();
    }
}
