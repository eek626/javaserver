package com.richardcheng.endpoint;

import com.richardcheng.FileHelper.AllowedFileTypes;
import com.richardcheng.FileHelper.FileReadHelper;
import com.richardcheng.FileHelper.FileWriteHelper;
import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpRequest;
import com.richardcheng.httpIO.HttpResponse;
import com.richardcheng.utils.SHAEncoder;

import java.util.Hashtable;

public class DynamicEndpoint implements IEndpoint {
    private Hashtable<String, Object> directoryList;
    private String path;
    private FileReadHelper fileReadHelper;
    private FileWriteHelper fileWriteHelper;
    private String fileName;
    private Hashtable<String, String> allowedMethods;
    private Hashtable<String, String> imageAllowedMethods;
    private Hashtable<String, String> textAllowedMethods;
    private Hashtable<String, Hashtable<String, String>> endpointType;
    private String fileType;
    private boolean isText;
    private String statusCode;
    private String message;
    private byte[] contentBytes;
    private byte[] imageHeader;
    private byte[] completeResponse;

    public DynamicEndpoint (Hashtable<String, Object> directoryList,
                            String path,
                            FileReadHelper fileReadHelper,
                            FileWriteHelper fileWriteHelper) {
        this.directoryList = directoryList;
        this.path = path;
        this.fileReadHelper = fileReadHelper;
        this.fileWriteHelper = fileWriteHelper;

        imageAllowedMethods = new Hashtable<>();
        imageAllowedMethods.put(HttpMethod.Get, HttpMethod.Status200);

        textAllowedMethods = new Hashtable<>();
        textAllowedMethods.put(HttpMethod.Get, HttpMethod.Status200);
        textAllowedMethods.put(HttpMethod.Patch, HttpMethod.Status204);

        endpointType = new Hashtable<>();
        endpointType.put(AllowedFileTypes.File, textAllowedMethods);
        endpointType.put(AllowedFileTypes.Gif, imageAllowedMethods);
        endpointType.put(AllowedFileTypes.Jpeg, imageAllowedMethods);
        endpointType.put(AllowedFileTypes.Jpg, imageAllowedMethods);
        endpointType.put(AllowedFileTypes.Png, imageAllowedMethods);
        endpointType.put(AllowedFileTypes.Txt, textAllowedMethods);
    }

    public byte[] route(HttpRequest httpRequest) {
        fileTypeHelper(httpRequest.getEndpoint());

        String httpMethod = httpRequest.getMethod();

        if (allowedMethods == null) {
            return HttpResponse.statusLine(HttpMethod.Status403).getBytes();
        }

        statusCode = allowedMethods.get(httpMethod);
        if (statusCode == null) {
            return HttpResponse.statusLine(HttpMethod.Status405).getBytes();
        }

        readFile(httpRequest);

        if (isText) {
            message = new String(contentBytes);
            if (etagHelper(httpRequest)) {
                return HttpResponse.statusLine(statusCode).getBytes();
            }
        } else {
            imageResponseHelper();
            return completeResponse;
        }

        return HttpResponse.completeResponse(statusCode, message).getBytes();
    }

    public void fileTypeHelper(String endpoint) {
        fileType = getFileExtension(endpoint);
        allowedMethods = endpointType.get(fileType);
        fileName = endpoint;
        if (fileType.equals("file") || fileType.equals("txt")) {
            isText = true;
        } else {
            isText = false;
        }
    }

    private String getFileExtension(String endpoint) {
        String[] nameSplit = endpoint.split("\\.");
        if (nameSplit.length == 1) {
            return "file";
        }
        return nameSplit[1];
    }

    private void readFile(HttpRequest httpRequest) {
        fileReadHelper.init(path + fileName);
        rangeHelper(httpRequest);
        contentBytes = fileReadHelper.readBytes();
    }

    private void rangeHelper(HttpRequest httpRequest) {
        if (httpRequest.isRange()) {
            statusCode = HttpMethod.Status206;
            fileReadHelper.parseRange(httpRequest.getRange());
            httpRequest.resetRange();
        }
    }

    private boolean etagHelper(HttpRequest httpRequest) {
        if (httpRequest.isEtag()) {
            String encodedMessage = SHAEncoder.encode(message, "SHA1");

            if (encodedMessage.equals(httpRequest.getEtag())) {
                fileWriteHelper.init(path + fileName);
                fileWriteHelper.write(httpRequest.getData());
                return true;
            }
        }

        return false;
    }

    private void imageResponseHelper() {
        imageHeader = HttpResponse.imageHeaderResponse(statusCode, fileType, contentBytes.length).getBytes();
        completeResponse = new byte[imageHeader.length + contentBytes.length];
        System.arraycopy(imageHeader, 0, completeResponse, 0, imageHeader.length);
        System.arraycopy(contentBytes, 0, completeResponse, imageHeader.length, contentBytes.length);
    }
}
