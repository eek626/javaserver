package com.richardcheng.endpoint;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpRequest;
import com.richardcheng.httpIO.HttpResponse;

public class InvalidEndpoint implements IEndpoint {

    public InvalidEndpoint() {
    }

    public byte[] route(HttpRequest httpRequest) {
        System.out.println("User bad endpoint: " + httpRequest.getEndpoint());

        return HttpResponse.statusLine(HttpMethod.Status404).getBytes();
    }
}
