package com.richardcheng.endpoint;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpRequest;
import com.richardcheng.httpIO.HttpResponse;
import com.richardcheng.presenter.Presenter;

import java.util.Hashtable;

public class RootEndpoint implements IEndpoint {
    private Hashtable<String, String> allowedMethods;
    private Presenter presenter;

    public RootEndpoint(Presenter presenter) {
        allowedMethods = new Hashtable<>();
        allowedMethods.put(HttpMethod.Get, HttpMethod.Status200);
        allowedMethods.put(HttpMethod.Head, HttpMethod.Status200);
        this.presenter = presenter;
    }

    public byte[] route(HttpRequest httpRequest) {
        String httpMethod = httpRequest.getMethod();
        String statusCode = allowedMethods.get(httpMethod);

        if (statusCode == null) {
            return HttpResponse.statusLine(HttpMethod.Status405).getBytes();
        }

        if (httpMethod.equals(HttpMethod.Head)) {
            return HttpResponse.statusLine(statusCode).getBytes();
        }

        return HttpResponse.completeResponse(statusCode, presenter.view()).getBytes();
    }
}
