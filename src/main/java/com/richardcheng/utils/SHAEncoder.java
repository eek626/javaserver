package com.richardcheng.utils;

import java.security.MessageDigest;

public class SHAEncoder {
    public static String encode(String plainText, String algorithmType) {
        String encoded = "";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithmType);
            messageDigest.update(plainText.getBytes());
            byte[] bytes = messageDigest.digest();
            encoded = bytesToHexString(bytes);
        } catch (Exception e) {
            System.out.println("Error encoding. Return empty string.");
            encoded = "";
        }
        return encoded;
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : bytes) {
            stringBuilder.append(String.format("%02x", b));
        }
        return stringBuilder.toString();
    }
}
