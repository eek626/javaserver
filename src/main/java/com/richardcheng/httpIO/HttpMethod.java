package com.richardcheng.httpIO;

public class HttpMethod {
    public static final String Get = "GET";
    public static final String Put = "PUT";
    public static final String Post = "POST";
    public static final String Delete = "DELETE";
    public static final String Head = "HEAD";
    public static final String Options = "OPTIONS";
    public static final String Patch = "PATCH";

    public static final String Status200 = "200 OK";
    public static final String Status204 = "204 No Content";
    public static final String Status206 = "206 Partial Content";
    public static final String Status302 = "302 Found";
    public static final String Status401 = "401 Unauthorized";
    public static final String Status403 = "403 Forbidden";
    public static final String Status404 = "404 Not Found";
    public static final String Status405 = "405 Method Not Allowed";
    public static final String Status418 = "418 I'm a teapot";
}
