package com.richardcheng.httpIO;

import java.util.Hashtable;

public class HttpResponse {
    private static String HEADER = "HTTP/1.1";
    private static String SPACE = " ";
    private static String CARRIAGERETURNLINEFEED = "\r\n";
    private static String COMMA = ",";

    public static String statusLine(String statusCode) {
        return HEADER + SPACE + statusCode + CARRIAGERETURNLINEFEED;
    }

    public static String entityHeader(int bodyLength) {
        return contentType() + contentLength(bodyLength);
    }

    private static String contentType() {
        return "Content-Type:" + SPACE + "text/html" + CARRIAGERETURNLINEFEED;
    }

    private static String contentLength(int bodyLength) {
        return "Content-Length:" + SPACE + Integer.toString(bodyLength) + CARRIAGERETURNLINEFEED;
    }

    public static String allowHeader(Hashtable<String, String> allowedMethods) {
        String allowed = "Allow: ";

        for (String method : allowedMethods.keySet()) {
            allowed += method + COMMA;
        }

        return allowed.replaceAll(",$", "") + CARRIAGERETURNLINEFEED;
    }

    public static String location(int port) {
        return "Location: http://localhost:" + Integer.toString(port) + "/" + CARRIAGERETURNLINEFEED;
    }

    public static String authHeader() {
        return "WWW-Authenticate: Basic" + CARRIAGERETURNLINEFEED;
    }

    public static String completeResponse(String statusCode, String message) {
        return statusLine(statusCode) + entityHeader(message.length()) + CARRIAGERETURNLINEFEED + message;
    }

    private static String imageContentType(String fileType) {
        return "Content-Type:" + SPACE + "image/" + fileType + CARRIAGERETURNLINEFEED;
    }

    private static String imageEntityHeader(String fileType, int imageSize) {
        return imageContentType(fileType) + contentLength(imageSize);
    }

    public static String imageHeaderResponse(String code, String fileType, int length) {
        return statusLine(code) + imageEntityHeader(fileType, length) + CARRIAGERETURNLINEFEED;
    }
}
