package com.richardcheng.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SHAEncoderTest {
    private String plainText;
    private String expectedSHA1;

    @Before
    public void setup() {
        plainText = "richard";
    }

    @Test
    public void encode_ReturnsSHA1_encodedString() {
        expectedSHA1 = "320bca71fc381a4a025636043ca86e734e31cf8b";

        String actualSHA1 = SHAEncoder.encode(plainText, "SHA1");

        Assert.assertEquals(expectedSHA1, actualSHA1);
    }

    @Test
    public void encode_MessageDigest_getInstance_throwsException_returnEmptyString() {
        expectedSHA1 = "";

        String actualSHA1 = SHAEncoder.encode(plainText, "bad_algorithm");

        Assert.assertEquals(expectedSHA1, actualSHA1);
    }
}