package com.richardcheng.utils;

import org.junit.Assert;
import org.junit.Test;

public class URLDecoderTest {
    @Test
    public void decode_ShouldDecode_EncodedUrl() {
        String encodedString = "?variable_1=Operators%20%3C%2C%20%3E%2C%20%3D%2C%20!%3D%3B%20%2B%2C%20-%2C%20*%2C%20%26%2C%20%40" +
                                "%2C%20%23%2C%20%24%2C%20%5B%2C%20%5D%3A%20%22is%20that%20all%22%3F&variable_2=stuff";
        String expectedResult = "variable_1 = Operators <, >, =, !=; +, -, *, &, @, #, $, [, ]: \"is that all\"? variable_2 = stuff";

        URLDecoder subject = new URLDecoder();

        String actualResult = subject.decode(encodedString);

        Assert.assertEquals(expectedResult, actualResult);
    }
}