package com.richardcheng.httpIO;

import org.junit.Assert;
import org.junit.Test;

import java.util.Hashtable;

public class HttpResponseTest {
    private String code;
    private String expectedResult;
    private String actualResult;

    @Test
    public void statusLine_Returns200_IfMatch() {
        code = HttpMethod.Status200;
        expectedResult = "HTTP/1.1 200 OK\r\n";

        actualResult = HttpResponse.statusLine(code);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void statusLine_Returns404_IfMatch() {
        code = HttpMethod.Status404;
        expectedResult = "HTTP/1.1 404 Not Found\r\n";

        actualResult = HttpResponse.statusLine(code);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void statusLine_Returns405_IfMatch() {
        code = HttpMethod.Status405;
        expectedResult = "HTTP/1.1 405 Method Not Allowed\r\n";

        actualResult = HttpResponse.statusLine(code);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void statusLine_Returns418_IfMatch() {
        code = HttpMethod.Status418;
        expectedResult = "HTTP/1.1 418 I'm a teapot\r\n";

        actualResult = HttpResponse.statusLine(code);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void entityHeader_ReturnsHeader_WithGiven_BodyLength() {
        int bodyLength = 100;
        expectedResult = "Content-Type: text/html\r\nContent-Length: 100\r\n";

        actualResult = HttpResponse.entityHeader(bodyLength);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void allowHeader_Returns_HeaderWith_AllowedMethods() {
        Hashtable<String, String> allowedMethods = new Hashtable<>();
        allowedMethods.put(HttpMethod.Get, HttpMethod.Status200);
        allowedMethods.put(HttpMethod.Options, HttpMethod.Status200);
        expectedResult = "Allow: GET,OPTIONS\r\n";

        actualResult = HttpResponse.allowHeader(allowedMethods);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void location_Returns_Root_URL() {
        int port = 5000;
        expectedResult = "Location: http://localhost:" + Integer.toString(port) + "/\r\n";

        actualResult = HttpResponse.location(port);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void authHeader_Returns_authMessage() {
        expectedResult = "WWW-Authenticate: Basic\r\n";

        actualResult = HttpResponse.authHeader();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void completeResponse_Returns_StatusLine_EntityHeader_Message() {
        String message = "HELLO";
        expectedResult = "HTTP/1.1 200 OK\r\n" +
                "Content-Type: text/html\r\nContent-Length: 5\r\n" +
                "\r\n" +
                "HELLO";

        actualResult = HttpResponse.completeResponse(HttpMethod.Status200, message);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void imageHeaderResponse_Returns_StatusLine_ImageEntityHeader() {
        expectedResult = "HTTP/1.1 200 OK\r\n" +
                "Content-Type: image/gif\r\nContent-Length: 11\r\n" +
                "\r\n";

        actualResult = HttpResponse.imageHeaderResponse(HttpMethod.Status200, "gif", 11);

        Assert.assertEquals(expectedResult, actualResult);
    }
}