package com.richardcheng.httpIO;

import org.junit.Assert;
import org.junit.Test;

public class HttpMethodTest {
    @Test
    public void constants_are_correct() {
        Assert.assertEquals("GET", HttpMethod.Get);
        Assert.assertEquals("PUT", HttpMethod.Put);
        Assert.assertEquals("POST", HttpMethod.Post);
        Assert.assertEquals("DELETE", HttpMethod.Delete);
        Assert.assertEquals("HEAD", HttpMethod.Head);
        Assert.assertEquals("OPTIONS", HttpMethod.Options);
        Assert.assertEquals("PATCH", HttpMethod.Patch);

        Assert.assertEquals("200 OK", HttpMethod.Status200);
        Assert.assertEquals("204 No Content", HttpMethod.Status204);
        Assert.assertEquals("206 Partial Content",HttpMethod.Status206);
        Assert.assertEquals("302 Found", HttpMethod.Status302);
        Assert.assertEquals("401 Unauthorized", HttpMethod.Status401);
        Assert.assertEquals("403 Forbidden", HttpMethod.Status403);
        Assert.assertEquals("404 Not Found", HttpMethod.Status404);
        Assert.assertEquals("405 Method Not Allowed", HttpMethod.Status405);
        Assert.assertEquals("418 I'm a teapot", HttpMethod.Status418);
    }
}
