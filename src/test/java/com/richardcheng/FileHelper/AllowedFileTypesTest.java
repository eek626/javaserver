package com.richardcheng.FileHelper;

import org.junit.Assert;
import org.junit.Test;

public class AllowedFileTypesTest {
    @Test
    public void constants_are_correct() {
        Assert.assertEquals("file", AllowedFileTypes.File);
        Assert.assertEquals("gif", AllowedFileTypes.Gif);
        Assert.assertEquals("jpeg", AllowedFileTypes.Jpeg);
        Assert.assertEquals("jpg", AllowedFileTypes.Jpg);
        Assert.assertEquals("png", AllowedFileTypes.Png);
        Assert.assertEquals("txt", AllowedFileTypes.Txt);
    }

}