package com.richardcheng.FileHelper;

import com.richardcheng.FileHelper.mock.MockBufferedWriter;
import com.richardcheng.FileHelper.mock.MockBufferedWriterThrowIOException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriteHelperTest {
    private String path;
    private FileWriteHelper subject;
    private File file;
    private FileWriter fileWriter;

    @Before
    public void setup() {
        path = System.getProperty("user.dir") + "/test.txt";
        subject = new FileWriteHelper();
        file = new File(path);
    }

    @Test
    public void init_setupFile_andFileWriter() {
        subject.init(path);

        Assert.assertNotNull(subject.getBufferedWriter());
    }

    @Test
    public void init_bufferWriterIsNull_whenfileWriter_throwsIOException() {
        subject.init(".");

        Assert.assertNull(subject.getBufferedWriter());
    }

    @Test
    public void write_success() {
        try {
            fileWriter = new FileWriter(file, false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        subject.setBufferedWriter(new MockBufferedWriter(fileWriter));

        boolean actualResult = subject.write("hi");

        Assert.assertEquals(true, actualResult);
    }

    @Test
    public void write_throwsException() {
        try {
            fileWriter = new FileWriter(file, false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        subject.setBufferedWriter(new MockBufferedWriterThrowIOException(fileWriter));

        boolean actualResult = subject.write("hi");

        Assert.assertEquals(false, actualResult);
    }
}