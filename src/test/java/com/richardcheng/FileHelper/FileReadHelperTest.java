package com.richardcheng.FileHelper;

import com.richardcheng.FileHelper.mock.MockFile;
import com.richardcheng.FileHelper.mock.MockFileInputStream;
import com.richardcheng.FileHelper.mock.MockFileInputStreamThrowIOException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;

public class FileReadHelperTest {
    private FileReadHelper subject;

    @Before
    public void setup() {
        subject = new FileReadHelper();
        subject.setFileSize(10);
    }

    @Test
    public void init_setsFile_andsetSizes() {
        String path = System.getProperty("user.dir") + "/README.md";

        subject.init(path);

        Assert.assertEquals(subject.getReadSize(), subject.getFileSize());
    }

    @Test
    public void initFile_fileinputstream_isNull_whenFileNotFoundException_occurs() {
        subject.init("IDontExist.txt");

        Assert.assertEquals(null, subject.getFileInputStream());
    }

    @Test
    public void parseRange_rangeEndsWithDash() {
        String testRange = "2-";
        int expectedStart = 2;
        int expectedStop = 9;
        int expectedReadSize = 8;

        subject.parseRange(testRange);

        Assert.assertEquals(expectedStart, subject.getStart());
        Assert.assertEquals(expectedStop, subject.getStop());
        Assert.assertEquals(expectedReadSize, subject.getReadSize());
    }

    @Test
    public void parseRange_rangeStartsWithDash() {
        String testRange = "-4";
        int expectedStart = 6;
        int expectedStop = 9;
        int expectedReadSize = 4;

        subject.parseRange(testRange);

        Assert.assertEquals(expectedStart, subject.getStart());
        Assert.assertEquals(expectedStop, subject.getStop());
        Assert.assertEquals(expectedReadSize, subject.getReadSize());
    }

    @Test
    public void parseRange_rangeContainsDash() {
        String testRange = "1-4";
        int expectedStart = 1;
        int expectedStop = 4;
        int expectedReadSize = 4;

        subject.parseRange(testRange);

        Assert.assertEquals(expectedStart, subject.getStart());
        Assert.assertEquals(expectedStop, subject.getStop());
        Assert.assertEquals(expectedReadSize, subject.getReadSize());
    }

    @Test
    public void parseRange_rangeNoDash() {
        String testRange = "4";
        int expectedStart = 4;
        int expectedStop = 9;
        int expectedReadSize = 6;

        subject.parseRange(testRange);

        Assert.assertEquals(expectedStart, subject.getStart());
        Assert.assertEquals(expectedStop, subject.getStop());
        Assert.assertEquals(expectedReadSize, subject.getReadSize());
    }

    @Test
    public void parseRange_nonNumberRange_throwsNumberFormatException_setRange_toZero() {
        String testRange = "x-a";
        int expectedReadSize = 0;

        subject.parseRange(testRange);

        Assert.assertEquals(expectedReadSize, subject.getReadSize());
    }

    @Test
    public void readbytes_ReturnsContentsOfMockFile() {
        String path = System.getProperty("user.dir") + "/README.md";
        MockFile mockFile = new MockFile(path);
        MockFileInputStream mockFileInputStream = null;
        try{
            mockFileInputStream = new MockFileInputStream(mockFile);
        } catch(FileNotFoundException e){
        }
        subject.setFileInputStream(mockFileInputStream);
        subject.setStop(1);
        subject.setReadSize(2);

        byte[] actualContent = subject.readBytes();
        String contentString = new String(actualContent);

        Assert.assertEquals(2, actualContent.length);
        Assert.assertEquals("hi", contentString);
    }

    @Test
    public void readBytes_ThrowsException_ReturnsNull() {
        String path = System.getProperty("user.dir") + "/README.md";
        MockFile mockFile = new MockFile(path);
        MockFileInputStreamThrowIOException mockFileInputStreamThrowIOException = null;
        try{
            mockFileInputStreamThrowIOException = new MockFileInputStreamThrowIOException(mockFile);
        } catch(FileNotFoundException e){
        }
        subject.setFileInputStream(mockFileInputStreamThrowIOException);
        subject.setReadSize(2);
        byte[] expectedContent = null;

        byte[] actualContent = subject.readBytes();

        Assert.assertEquals(expectedContent, actualContent);
    }
}