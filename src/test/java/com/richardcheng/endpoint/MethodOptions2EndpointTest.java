package com.richardcheng.endpoint;

import com.richardcheng.endpoint.mock.MockHttpRequestGet;
import com.richardcheng.endpoint.mock.MockHttpRequestNoMatch;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MethodOptions2EndpointTest {
    private MethodOptions2Endpoint subject;
    private String expectedResponse;
    private String actualResponse;
    private byte[] byteArray;

    @Before
    public void setup() {
        subject = new MethodOptions2Endpoint();
    }

    @Test
    public void route_Returns200Response_IfMethodMatch() {
        MockHttpRequestGet httpRequest = new MockHttpRequestGet();
        expectedResponse = "HTTP/1.1 200 OK\r\nAllow: GET,OPTIONS\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void route_Returns405Response_IfNo_MethodMatch() {
        MockHttpRequestNoMatch httpRequest = new MockHttpRequestNoMatch();
        expectedResponse = "HTTP/1.1 405 Method Not Allowed\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }
}