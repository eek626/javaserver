package com.richardcheng.endpoint;

import com.richardcheng.endpoint.mock.MockFileReadHelper;
import com.richardcheng.endpoint.mock.MockFileWriteHelper;
import com.richardcheng.endpoint.mock.MockHttpRequestGet;
import com.richardcheng.endpoint.mock.MockHttpRequestGetEtag;
import com.richardcheng.endpoint.mock.MockHttpRequestGetFile;
import com.richardcheng.endpoint.mock.MockHttpRequestGetImage;
import com.richardcheng.endpoint.mock.MockHttpRequestGetMovie;
import com.richardcheng.endpoint.mock.MockHttpRequestGetOnly;
import com.richardcheng.endpoint.mock.MockHttpRequestGetPartial;
import com.richardcheng.endpoint.mock.MockHttpRequestPut;
import com.richardcheng.endpoint.mock.MockHttpRequestPutImage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Hashtable;

public class DynamicEndpointTest {
    private Hashtable<String, Object> directoryList;
    private DynamicEndpoint subject;

    @Before
    public void setup() {
        directoryList = new Hashtable<>();
        directoryList.put("file1", 1);
        directoryList.put("image.png", 1);
        directoryList.put("partial_content.txt", 1);
        directoryList.put("movie.mov", 1);

        subject = new DynamicEndpoint(directoryList, "path", new MockFileReadHelper(), new MockFileWriteHelper());
    }

    @Test
    public void route_Returns403_ifFileTypeUnsupported() {
        MockHttpRequestGetMovie httpRequest = new MockHttpRequestGetMovie();

        byte[] byteArray = subject.route(httpRequest);
        String actualRouteResponse = new String(byteArray);

        Assert.assertEquals(true, actualRouteResponse.contains("HTTP/1.1 403 Forbidden"));
    }

    @Test
    public void route_Returns405_requestNotAllowed() {
        MockHttpRequestPutImage httpRequest = new MockHttpRequestPutImage();
        String expectedRouteResponse = "HTTP/1.1 405 Method Not Allowed\r\n";

        byte[] byteArray = subject.route(httpRequest);
        String actualRouteResponse = new String(byteArray);

        Assert.assertEquals(expectedRouteResponse, actualRouteResponse);
    }

    @Test
    public void route_Returns206_partialContent() {
        MockHttpRequestGetPartial httpRequest = new MockHttpRequestGetPartial();

        byte[] byteArray = subject.route(httpRequest);
        String actualRouteResponse = new String(byteArray);

        Assert.assertEquals(true, actualRouteResponse.contains("HTTP/1.1 206 Partial Content"));
    }

    @Test
    public void route_Returns200_getTextFile() {
        MockHttpRequestGetFile httpRequest = new MockHttpRequestGetFile();

        byte[] byteArray = subject.route(httpRequest);
        String actualRouteResponse = new String(byteArray);

        Assert.assertEquals(true, actualRouteResponse.contains("HTTP/1.1 200 OK"));
    }

    @Test
    public void route_Returns200_getImageFile() {
        MockHttpRequestGetImage httpRequest = new MockHttpRequestGetImage();

        byte[] byteArray = subject.route(httpRequest);
        String actualRouteResponse = new String(byteArray);

        Assert.assertEquals(true, actualRouteResponse.contains("HTTP/1.1 200 OK"));
    }

    @Test
    public void route_Etag_noMatch() {
        MockHttpRequestGetEtag httpRequest = new MockHttpRequestGetEtag();

        byte[] byteArray = subject.route(httpRequest);
        String actualRouteResponse = new String(byteArray);

        Assert.assertEquals(true, actualRouteResponse.contains("HTTP/1.1 200 OK"));
    }
}