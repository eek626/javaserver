package com.richardcheng.endpoint.mock;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpRequest;

public class MockHttpRequestGetFile extends HttpRequest {
    public String getMethod() {
        return HttpMethod.Get;
    }

    public String getEndpoint() {
        return "file1";
    }
}
