package com.richardcheng.endpoint.mock;

import com.richardcheng.httpIO.HttpRequest;

public class MockHttpRequestGetMovie extends HttpRequest {
    public String getMethod() {
        return "GET";
    }

    public String getEndpoint() {
        return "movie.mov";
    }
}
