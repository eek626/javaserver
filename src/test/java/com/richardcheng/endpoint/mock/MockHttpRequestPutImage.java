package com.richardcheng.endpoint.mock;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpRequest;

public class MockHttpRequestPutImage extends HttpRequest {
    public String getMethod() {
        return HttpMethod.Put;
    }

    public String getEndpoint() {
        return "image.png";
    }
}
