package com.richardcheng.endpoint.mock;

import com.richardcheng.httpIO.HttpMethod;
import com.richardcheng.httpIO.HttpRequest;

public class MockHttpRequestGetPartial extends HttpRequest {
    public String getMethod() {
        return HttpMethod.Get;
    }

    public String getEndpoint() {
        return "partial_content.txt";
    }

    public String getRange() {
        return "0-4";
    }

    public boolean isRange() {
        return true;
    }
}
