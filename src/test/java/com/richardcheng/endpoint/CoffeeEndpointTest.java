package com.richardcheng.endpoint;

import com.richardcheng.endpoint.mock.MockHttpRequestGet;
import com.richardcheng.endpoint.mock.MockHttpRequestPost;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CoffeeEndpointTest {
    private CoffeeEndpoint subject;
    private String expectedResponse;
    private String actualResponse;
    private byte[] byteArray;

    @Before
    public void setup() {
        subject = new CoffeeEndpoint();
    }

    @Test
    public void route_Returns418Response() {
        MockHttpRequestGet httpRequest = new MockHttpRequestGet();
        expectedResponse = "HTTP/1.1 418 I'm a teapot\r\nContent-Type: text/html\r\nContent-Length: 12\r\n\r\nI'm a teapot";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void route_Returns405() {
        MockHttpRequestPost httpRequest = new MockHttpRequestPost();
        expectedResponse = "HTTP/1.1 405 Method Not Allowed\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }
}