package com.richardcheng.endpoint;

import com.richardcheng.endpoint.mock.MockHttpRequestGet;
import org.junit.Assert;
import org.junit.Test;

public class InvalidEndpointTest {
    @Test
    public void route_ReturnsResponse() {
        InvalidEndpoint subject = new InvalidEndpoint();
        MockHttpRequestGet httpRequest = new MockHttpRequestGet();
        String expectedRouteResponse = "HTTP/1.1 404 Not Found\r\n";

        byte[] byteArray = subject.route(httpRequest);
        String actualRouteResponse = new String(byteArray);

        Assert.assertEquals(expectedRouteResponse, actualRouteResponse);
    }
}