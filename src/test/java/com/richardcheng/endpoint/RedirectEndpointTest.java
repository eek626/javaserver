package com.richardcheng.endpoint;

import com.richardcheng.endpoint.mock.MockHttpRequestGet;
import com.richardcheng.endpoint.mock.MockHttpRequestPost;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RedirectEndpointTest {
    private RedirectEndpoint subject;
    private int port;
    private String expectedResponse;
    private String actualResponse;
    private byte[] byteArray;

    @Before
    public void setup() {
        port = 5000;
        subject = new RedirectEndpoint(port);
    }

    @Test
    public void route_Returns302Response() {
        MockHttpRequestGet httpRequest = new MockHttpRequestGet();
        expectedResponse = "HTTP/1.1 302 Found\r\nLocation: http://localhost:5000/\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void route_Returns405Response() {
        MockHttpRequestPost httpRequest = new MockHttpRequestPost();
        expectedResponse = "HTTP/1.1 405 Method Not Allowed\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

}