package com.richardcheng.endpoint;

import com.richardcheng.endpoint.mock.*;
import com.richardcheng.javaserver.FileHelper;
import com.richardcheng.presenter.Presenter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Hashtable;

public class RootEndpointTest {
    private Hashtable<String, Object> directoryList;
    private RootEndpoint subject;
    private String expectedResponse;
    private String actualResponse;
    private byte[] byteArray;

    @Before
    public void setup() {
        File[] files = { new File("file1"), new File("file2") };
        FileHelper fileHelper = new FileHelper(files);

        directoryList = fileHelper.listMap();
        subject = new RootEndpoint(new Presenter(directoryList));
    }

    @Test
    public void route_Returns200Response_IfGETRequestMethod_IsFound() {
        MockHttpRequestGet httpRequest = new MockHttpRequestGet();
        expectedResponse = "HTTP/1.1 200 OK\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(true, actualResponse.contains(expectedResponse));
        Assert.assertEquals(true, actualResponse.contains("href"));
        Assert.assertEquals(true, actualResponse.contains("file1"));
        Assert.assertEquals(true, actualResponse.contains("file2"));
    }

    @Test
    public void route_Returns200Response_IfHEADRequestMethod_IsFound() {
        MockHttpRequestHead httpRequest = new MockHttpRequestHead();
        expectedResponse = "HTTP/1.1 200 OK\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void route_Returns405Response_IfRequestMethod_NotFound() {
        MockHttpRequestPut httpRequest = new MockHttpRequestPut();
        expectedResponse = "HTTP/1.1 405 Method Not Allowed\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }
}