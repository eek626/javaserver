package com.richardcheng.endpoint;

import com.richardcheng.endpoint.mock.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FormEndpointTest {
    private FormEndpoint subject;
    private String expectedResponse;
    private String actualResponse;
    private byte[] byteArray;

    @Before
    public void setup() {
        subject = new FormEndpoint();
        expectedResponse = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: 0\r\n\r\n";
    }

    @Test
    public void route_Returns200Response_IfPOSTMethodMatch_WithData_WhenDataEmpty() {
        MockHttpRequestPost httpRequest = new MockHttpRequestPost();
        expectedResponse = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: 9\r\n\r\ndata=odey";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void route_Returns200Response_IfPostMethodMatch_NoDataUpdate_WhenDataExist() {
        MockHttpRequestPost postHttpRequest = new MockHttpRequestPost();
        MockHttpRequestPut putHttpRequest = new MockHttpRequestPut();
        MockHttpRequestPost secondPostHttpRequest = new MockHttpRequestPost();
        expectedResponse = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: 13\r\n\r\ndata=garfield";

        byteArray = subject.route(postHttpRequest);
        String firstRouteResponse = new String(byteArray);
        byteArray = subject.route(putHttpRequest);
        String secondRouteResponse = new String(byteArray);
        byteArray = subject.route(secondPostHttpRequest);
        String actualRouteResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualRouteResponse);
        Assert.assertEquals(secondRouteResponse, actualRouteResponse);
        Assert.assertNotEquals(firstRouteResponse, actualRouteResponse);
    }

    @Test
    public void route_Returns200Response_IfPUTMethodMatch_WithData_WhenUpdatingData() {
        MockHttpRequestPut httpRequest = new MockHttpRequestPut();
        MockHttpRequestPost httpRequestPost = new MockHttpRequestPost();
        expectedResponse = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: 13\r\n\r\ndata=garfield";

        byteArray = subject.route(httpRequestPost);

        String setupResponse = new String(byteArray);
        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
        Assert.assertNotEquals(setupResponse, actualResponse);
    }

    @Test
    public void route_Returns200Response_IfPUTMethodMatch_NoData_WhenNo_ExistingData() {
        MockHttpRequestPut httpRequest = new MockHttpRequestPut();

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void route_Returns200Response_OnlyIfGETMethod() {
        MockHttpRequestGet httpRequest = new MockHttpRequestGet();

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void route_Returns405Response_IfNo_MethodMatch() {
        MockHttpRequestNoMatch httpRequest = new MockHttpRequestNoMatch();
        expectedResponse = "HTTP/1.1 405 Method Not Allowed\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void route_Returns200Response_Only_IfDELETEMethod() {
        MockHttpRequestDelete httpRequest = new MockHttpRequestDelete();

        byteArray = subject.route(httpRequest);
        String actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }
}