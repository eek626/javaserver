package com.richardcheng.endpoint;

import org.junit.Assert;
import org.junit.Test;

import java.util.Hashtable;

public class EndpointFactoryTest {
    @Test
    public void create_shouldReturn_HashTable_ofEndpoints() {
        int port = 5000;
        String path = "some_path";
        Hashtable<String, Object> directoryList = new Hashtable<>();

        Hashtable<String, IEndpoint> subject = EndpointFactory.create(port, path, directoryList);

        Assert.assertEquals(RootEndpoint.class, subject.get("root").getClass());
        Assert.assertEquals(FormEndpoint.class, subject.get("form").getClass());
        Assert.assertEquals(CoffeeEndpoint.class, subject.get("coffee").getClass());
        Assert.assertEquals(TeaEndpoint.class, subject.get("tea").getClass());
        Assert.assertEquals(MethodOptionsEndpoint.class, subject.get("method_options").getClass());
        Assert.assertEquals(MethodOptions2Endpoint.class, subject.get("method_options2").getClass());
        Assert.assertEquals(RedirectEndpoint.class, subject.get("redirect").getClass());
        Assert.assertEquals(LogsEndpoint.class, subject.get("logs").getClass());
        Assert.assertEquals(ParametersEndpoint.class, subject.get("parameters").getClass());
        Assert.assertEquals(DynamicEndpoint.class, subject.get("dynamic").getClass());
        Assert.assertEquals(InvalidEndpoint.class, subject.get("invalid").getClass());
    }

}