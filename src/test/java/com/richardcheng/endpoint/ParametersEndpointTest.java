package com.richardcheng.endpoint;

import com.richardcheng.endpoint.mock.MockHttpRequestGet;
import com.richardcheng.endpoint.mock.MockHttpRequestPut;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ParametersEndpointTest {
    private ParametersEndpoint subject;
    private String expectedResponse;
    private String actualResponse;
    private byte[] byteArray;

    @Before
    public void setup() {
        subject = new ParametersEndpoint();
    }

    @Test
    public void route_Returns200() {
        MockHttpRequestGet httpRequest = new MockHttpRequestGet();
        expectedResponse = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: 2\r\n\r\n\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void route_Returns405() {
        MockHttpRequestPut httpRequest = new MockHttpRequestPut();
        expectedResponse = "HTTP/1.1 405 Method Not Allowed\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }
}