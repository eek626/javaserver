package com.richardcheng.endpoint;

import com.richardcheng.endpoint.mock.MockHttpRequestGet;
import com.richardcheng.endpoint.mock.MockHttpRequestGetNoAuth;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LogsEndpointTest {
    private LogsEndpoint subject;
    private String expectedResponse;
    private String actualResponse;
    private byte[] byteArray;

    @Before
    public void setup() {
        subject = new LogsEndpoint();
    }

    @Test
    public void route_Returns200withLogResponse_if_Authenticated() {
        MockHttpRequestGet httpRequest = new MockHttpRequestGet();
        expectedResponse = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: 17\r\n\r\nGET /log HTTP/1.1";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void route_Returns401withAuthHeaderResponse_if_notAuthenticated() {
        MockHttpRequestGetNoAuth httpRequest = new MockHttpRequestGetNoAuth();
        expectedResponse = "HTTP/1.1 401 Unauthorized\r\nWWW-Authenticate: Basic\r\n";

        byteArray = subject.route(httpRequest);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }
}