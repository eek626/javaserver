package com.richardcheng.javaserver.mock;

import com.richardcheng.endpoint.RootEndpoint;
import com.richardcheng.presenter.Presenter;

public class MockRootEndpointNoMatch extends RootEndpoint {

    public MockRootEndpointNoMatch(Presenter presenter) {
        super(presenter);
    }
}
