package com.richardcheng.javaserver.mock;

import com.richardcheng.endpoint.IEndpoint;
import com.richardcheng.javaserver.Controller;
import com.richardcheng.httpIO.HttpRequest;

import java.util.Hashtable;

public class MockController extends Controller {
    public MockController(Hashtable<String, IEndpoint> endpoints, Hashtable<String, Object> directoryList) {
        super(endpoints, directoryList);
    }

    public byte[] routeRequest(HttpRequest request) {
        return "response".getBytes();
    }
}
