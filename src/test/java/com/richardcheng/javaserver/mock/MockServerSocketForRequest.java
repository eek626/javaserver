package com.richardcheng.javaserver.mock;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;

public class MockServerSocketForRequest extends MockServerSocket {

    public MockServerSocketForRequest() throws IOException {
    }

    public Socket accept() throws IOException {
        MockSocketGetInputStreamThrowsIOException mockSocket;
        try {
            mockSocket = new MockSocketGetInputStreamThrowsIOException();
        } catch (IOException e) {
            throw new IOException(e);
        }
        return mockSocket;
    }
}
