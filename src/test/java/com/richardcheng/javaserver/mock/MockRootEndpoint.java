package com.richardcheng.javaserver.mock;

import com.richardcheng.endpoint.RootEndpoint;
import com.richardcheng.httpIO.HttpRequest;
import com.richardcheng.presenter.Presenter;

public class MockRootEndpoint extends RootEndpoint {

    public MockRootEndpoint(Presenter presenter) {
        super(presenter);
    }

    public byte[] route(HttpRequest httpRequest) {
        return "OK".getBytes();
    }
}
