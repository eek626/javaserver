package com.richardcheng.javaserver.mock;

import com.richardcheng.endpoint.InvalidEndpoint;
import com.richardcheng.httpIO.HttpRequest;

public class MockInvalidEndpoint extends InvalidEndpoint{

    public MockInvalidEndpoint() {
        super();
    }

    public byte[] route(HttpRequest httpRequest) {
        return "INVALID".getBytes();
    }

}
