package com.richardcheng.javaserver.mock;

import com.richardcheng.FileHelper.FileReadHelper;
import com.richardcheng.FileHelper.FileWriteHelper;
import com.richardcheng.endpoint.DynamicEndpoint;
import com.richardcheng.httpIO.HttpRequest;

import java.util.Hashtable;

public class MockDynamicEndpoint extends DynamicEndpoint {

    public MockDynamicEndpoint(Hashtable<String, Object> directoryList,
                               String path,
                               FileReadHelper fileReadHelper,
                               FileWriteHelper fileWriteHelper) {
        super(directoryList, path, fileReadHelper, fileWriteHelper);
    }

    public byte[] route(HttpRequest httpRequest) {
        return "DYNAMIC".getBytes();
    }
}
