package com.richardcheng.javaserver;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.Hashtable;

public class FileHelperTest {
    @Test
    public void listFiles_Returns_Hashtable_listOfFiles_InDirectory() {
        File[] files = { new File("file1"), new File("file2"), new File("file3") };
        FileHelper subject = new FileHelper(files);

        Hashtable<String, Object> actualResult = subject.listMap();

        Assert.assertEquals(true, actualResult.containsKey("file1"));
        Assert.assertEquals(true, actualResult.containsKey("file1"));
        Assert.assertEquals(true, actualResult.containsKey("file1"));
    }
}