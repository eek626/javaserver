package com.richardcheng.javaserver;

import com.richardcheng.endpoint.IEndpoint;
import com.richardcheng.javaserver.mock.*;
import com.richardcheng.presenter.Presenter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Hashtable;

public class ControllerTest {
    private Hashtable<String, IEndpoint> endpoints;
    private Hashtable<String, Object> directoryList;
    private Controller subject;
    private String expectedResponse;
    private String actualResponse;
    private byte[] byteArray;

    @Before
    public void setup() {
        endpoints = new Hashtable<>();
        endpoints.put("endpoint", new MockRootEndpoint(new Presenter()));
        endpoints.put("invalid", new MockInvalidEndpoint());
        endpoints.put("dynamic", new MockDynamicEndpoint(null, null, null, null));

        File[] files = { new File("file1"), new File("file2"), new File("file3") };
        FileHelper fileHelper = new FileHelper(files);

        directoryList = fileHelper.listMap();

        subject = new Controller(endpoints, directoryList);
    }

    @Test
    public void routeRequest_ReturnsRouteResponse_IfEndpointFound() {
        MockHttpRequest request = new MockHttpRequest();
        expectedResponse = "OK";

        byteArray = subject.routeRequest(request);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void routeRequest_ReturnsRouteResponse_IfEndpointNotFound() {
        MockBadEndpointHttpRequest request = new MockBadEndpointHttpRequest();
        expectedResponse = "INVALID";

        byteArray = subject.routeRequest(request);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void routeRequest_ReturnsRouteResponse_toDynamicEndpoint() {
        MockDynamicEndpointHttpRequest request = new MockDynamicEndpointHttpRequest();
        expectedResponse = "DYNAMIC";

        byteArray = subject.routeRequest(request);
        actualResponse = new String(byteArray);

        Assert.assertEquals(expectedResponse, actualResponse);
    }
}
