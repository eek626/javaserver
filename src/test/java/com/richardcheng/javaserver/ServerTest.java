package com.richardcheng.javaserver;

import com.richardcheng.javaserver.mock.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class ServerTest {
    private MockServerSocket mockServerSocket;
    private MockController mockController;
    private MockHttpRequest mockHttpRequest;
    private Server subject;
    private int port;
    private boolean production;

    @Before
    public void setup() {
        mockController = new MockController(null, null);
        mockHttpRequest = new MockHttpRequest();
        port = 5000;
        production = false;
    }

    @Test
    public void run_LoopsThrough_Accept_Request_Response_Methods() {
        try {
            mockServerSocket = new MockServerSocket();
        } catch (IOException e) {
        }
        subject = new Server(mockServerSocket, mockController, mockHttpRequest);

        subject.run(port, production);

        Assert.assertEquals(true, mockServerSocket.setReuseAddressCalled);
        Assert.assertEquals(true, mockServerSocket.bindCalled);
        Assert.assertEquals(true, mockHttpRequest.requestParsed);
    }

    @Test
    public void run_LoopsThrough_IfAcceptMethodGood_RequestFails_andSkipsResponse() {
        try {
            mockServerSocket = new MockServerSocket();
        } catch (IOException e) {
        }
        MockHttpRequestReturnFalse mockHttpRequest = new MockHttpRequestReturnFalse();
        subject = new Server(mockServerSocket, mockController, mockHttpRequest);

        subject.run(port, production);

        Assert.assertEquals(true, mockServerSocket.setReuseAddressCalled);
        Assert.assertEquals(true, mockServerSocket.bindCalled);
        Assert.assertEquals(false, mockHttpRequest.requestParsed);
    }

    @Test
    public void run_continues_when_serverSocket_bind_throwsIOException() {
        MockServerSocketBindThrowsException mockServerSocket = null;
        try {
            mockServerSocket = new MockServerSocketBindThrowsException();
        } catch (IOException e) {
        }
        subject = new Server(mockServerSocket, mockController, mockHttpRequest);

        subject.run(port, production);
    }

    @Test
    public void run_continues_when_serverSocket_accept_throwsIOException() {
        MockServerSocketAcceptThrowsException mockServerSocket;
        try {
            mockServerSocket = new MockServerSocketAcceptThrowsException();
        } catch (IOException e) {
            throw new RuntimeException();
        }
        subject = new Server(mockServerSocket, mockController, mockHttpRequest);

        subject.run(port, production);
    }

    @Test
    public void run_continues_when_request_throwsException() {
        MockServerSocketForRequest mockServerSocket;
        try {
            mockServerSocket = new MockServerSocketForRequest();
        } catch (IOException e) {
            throw new RuntimeException();
        }
        subject = new Server(mockServerSocket, mockController, mockHttpRequest);

        subject.run(port, production);
    }

    @Test
    public void run_continues_when_response_throwsException() {
        MockServerSocketForResponse mockServerSocket;
        try {
            mockServerSocket = new MockServerSocketForResponse();
        } catch (IOException e) {
            throw new RuntimeException();
        }
        subject = new Server(mockServerSocket, mockController, mockHttpRequest);

        subject.run(port, production);
    }

    @Test
    public void stop_closesSocket() {
        MockServerSocket mockServerSocket;
        try {
            mockServerSocket = new MockServerSocket();
        } catch (IOException e) {
            throw new RuntimeException();
        }
        subject = new Server(mockServerSocket, mockController, mockHttpRequest);

        subject.stop();

        Assert.assertEquals(true, mockServerSocket.closeCalled);
    }

    @Test
    public void stop_throwsException() {
        MockServerSocketForStop mockServerSocket;
        try {
            mockServerSocket = new MockServerSocketForStop();
        } catch (IOException e) {
            throw new RuntimeException();
        }
        subject = new Server(mockServerSocket, mockController, mockHttpRequest);

        subject.stop();
    }
}
