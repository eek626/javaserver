package com.richardcheng.javaserver;

import org.junit.Assert;
import org.junit.Test;

public class ServerArgumentHelperTest {
    @Test
    public void parseArgs_SetsObject_WithDefaultPort_andNoDirectoryPath_IfArgArrayEmpty() {
        String[] args = { "" };
        int expectedPort = 5000;
        String expectedPath = "";
        int expectedPathLength = 0;

        ServerArgumentHelper subject = new ServerArgumentHelper(args);

        Assert.assertEquals(expectedPort, subject.port());
        Assert.assertEquals(expectedPath, subject.path());
        Assert.assertEquals(expectedPathLength, subject.pathLength());
    }

    @Test
    public void parseArgs_SetsObject_WithSpecifiedPort_andNoDirectoryPath_IfPathNotProvided() {
        String[] args = { "-p", "3000" };
        int expectedPort = 3000;
        String expectedPath = "";
        int expectedPathLength = 0;

        ServerArgumentHelper subject = new ServerArgumentHelper(args);

        Assert.assertEquals(expectedPort, subject.port());
        Assert.assertEquals(expectedPath, subject.path());
        Assert.assertEquals(expectedPathLength, subject.pathLength());
    }

    @Test
    public void parseArgs_SetsObject_WithSpecifiedPath_andDefaultPort_IfPortNotProvided() {
        String[] args = { "-d", "/path" };
        int expectedPort = 5000;
        String expectedPath = "/path";
        int expectedPathLength = 5;

        ServerArgumentHelper subject = new ServerArgumentHelper(args);

        Assert.assertEquals(expectedPort, subject.port());
        Assert.assertEquals(expectedPath, subject.path());
        Assert.assertEquals(expectedPathLength, subject.pathLength());
    }

    @Test
    public void parseArgs_SetsObject_WithSpecifiedPortandPath_WherePathDoesntHavetoBeLast() {
        String[] args = { "-d", "/path", "-p", "3000" };
        int expectedPort = 3000;
        String expectedPath = "/path";
        int expectedPathLength = 5;

        ServerArgumentHelper subject = new ServerArgumentHelper(args);

        Assert.assertEquals(expectedPort, subject.port());
        Assert.assertEquals(expectedPath, subject.path());
        Assert.assertEquals(expectedPathLength, subject.pathLength());
    }

    @Test
    public void parseArgs_SetsObject_WithDefaultsIgnoringSpecifiedArgs_IfStringArg_LengthIsOddNumbered() {
        String[] args = { "5000", "-d", "/path"};
        int expectedPort = 5000;
        String expectedPath = "";
        int expectedPathLength = 0;

        ServerArgumentHelper subject = new ServerArgumentHelper(args);

        Assert.assertEquals(expectedPort, subject.port());
        Assert.assertEquals(expectedPath, subject.path());
        Assert.assertEquals(expectedPathLength, subject.pathLength());
    }
}